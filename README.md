#Algorithm Configuration Library 2.0

## Requirements for a Scenario

__Goal:__ only a small hand-selected set of scenarios

* scenario name has to be unique (also across different domains); name scheme: <algorithm>_<instance set>
* target algorithm uses the generic wrapper
* instance set sufficient large and provides instances features (trivial features are sufficient) for training and test instances
* fixed name scheme:

    * scenario.txt
    * params.pcs
    * wrapper.py
    * instance files: training.txt, test.txt, features.txt

* if possible, returned solution should be verified by wrapper
* if possible, the scenario should have a bibtex reference
 
## Install

AClib requires Python 3.5 (implemented under Anaconda 3.4) and Java >8

`pip install -r requirements.txt`

Some target algorithms may have further dependencies.


### Installation of Instances

Since the instance set is by far too large to upload on bitbucket (compressed 2.9GB), 
you can download the instances [here](http://aclib.net/aclib2/instances.tar.gz).

Please extract the instances (12.4GB) in the root directory of AClib2:

`tar xvfz instances.tar.gz`

## Requirements for AC procedures

### SMAC

* Java 8

### ParamILS

* ruby 1.9.3 (https://www.ruby-lang.org/de/news/2014/05/16/ruby-1-9-3-p547-released/)

### GGA

* precompiled on Ubuntu 14.04 LTS with gcc -- probably needs to be recompiled

## Example

### Configuration

To run a scenario call:

`python aclib2/aclib/run.py -s circuit_fuzz_cryptominisat -c SMAC2 -n 2 --env local`

It runs the scenario __circuit_fuzz_cryptominisat__ with 2 independent SMAC (v2) runs. 

To use the same resources for GGA, call:

`python aclib2/aclib/run.py -s circuit_fuzz_cryptominisat -c GGA -n 1 --ac_cores 2 --cores_per_job 2 --env local`

### Validation without Workers

To validate the runs (here training and test performance of final incumbent):

`python aclib2/aclib/validate.py -s circuit_fuzz_cryptominisat -c SMAC2 -n 2 --set TRAIN+TEST --mode INC --env local`


### Validation with Workers

To validate the runs (here performance over time with using workers on a MYSQL data base -- this will work only on our meta cluster in Freiburg. Send us an e-mail if you also want to use it on your system.):

`python aclib2/aclib/validate.py -s circuit_fuzz_cryptominisat -c SMAC2 -n 2 --set TEST --mode TIME --pool test_aclib2 --env local`

and to submit workers:

`python aclib2/aclib/submit_workers.py --pool test_aclib2 -n 10000 --time_limit 910`

To get read results from the database, run again:

`python aclib2/aclib/validate.py -s circuit_fuzz_cryptominisat -c SMAC2 -n 8 --set TEST --mode TIME --pool test_aclib2 --env local`

### Statistics and Plots

If you have validated your runs, you can run the following command to get some basic statistics and scatter plots:

`python aclib2/aclib/get_evaluation_stats.py` 

This script will look into "." for runs generated with the previous scripts.

If you have validated your runs over time (`--mode TIME`), you can plot the performance of the configurators over time:

`python aclib2/aclib/plot_perf_over_time.py -s circuit_fuzz_cryptominisat`

## Contact

Marius Lindauer lindauer@cs.uni-freiburg.de

## Issue Tracker

https://bitbucket.org/mlindauer/aclib2/issues

## Scenarios

| Scenario 							| Domain	| Configurators 		| #Params 	| #Instances 	| Budget 	|
| --------------------------------- | --------- | --------------------- |:---------:|:-------------:|:---------:|
| clasp-weighted-sequence		 	| ASP		| SMAC, ROAR, PILS		| 90		| 240/240	 	| 4d	 	|
| cplex_regions200	 				| MIP 		| SMAC, ROAR, PILS, GGA	| 73		| 1000/1000		| 2d	 	|
| lpg-zenotravel					| Planning 	| SMAC, ROAR, PILS	 	| 67		| 2000/2000		| 2d	 	|
| cryptominisat_circuit_fuzz		| SAT		| SMAC, ROAR, PILS, GGA	| 36		| 299/302		| 2d     	|
| probsat_7sat90					| SAT		| SMAC, ROAR, PILS, GGA?| 9			| 250/250		| 3h     	|
| spear_swgcp						| SAT		| SMAC, ROAR, PILS		| 26		| 1000/2000		| 5h     	|
| branin (multi-instance)			| BBOB		| SMAC, ROAR, 			| 1			| 76/75			| 1000 runs |