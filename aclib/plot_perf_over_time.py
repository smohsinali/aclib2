#!/usr/bin/env python

__author__ = "Marius Lindauer and Katharina Eggensperger"
__version__ = "0.0.1"
__license__ = "BSD"

from argparse import ArgumentParser
import glob
import sys

import matplotlib
import numpy as np
matplotlib.use('Agg')

from plottingscripts.utils import plot_util, read_util
from plottingscripts.plotting import plot_methods


def main():
    prog = "python plot_performance.py"
    description = "Plot a median trace with quantiles for multiple experiments"

    parser = ArgumentParser(description=description, prog=prog)

    # General Options
    parser.add_argument("-s", "--scenario",
                        required=True, help="scenario name")

    parser.add_argument("--logx", action="store_true", dest="logx",
                        default=False, help="Plot x on log scale")
    parser.add_argument("--logy", action="store_true", dest="logy",
                        default=False, help="Plot y on log scale")
    parser.add_argument("--ymax", dest="ymax", type=float,
                        default=None, help="Maximum of the y-axis")
    parser.add_argument("--ymin", dest="ymin", type=float,
                        default=None, help="Minimum of the y-axis")
    parser.add_argument("--xmax", dest="xmax", type=float,
                        default=None, help="Maximum of the x-axis")
    parser.add_argument("--xmin", dest="xmin", type=float,
                        default=None, help="Minimum of the x-axis")
    parser.add_argument("-S", "--save", dest="save",
                        default=None,
                        help="Save plot to a specifix destination? Otherwise "
                             "will be saved as ./<scenario>_perf_over_time.pdf")
    parser.add_argument("-t", "--title", dest="title",
                        default="", help="Optional supertitle for plot")
    parser.add_argument("--maxvalue", dest="maxvalue", type=float,
                        default=sys.maxsize,
                        help="Replace all values higher than this?")
    parser.add_argument("--agglomeration", dest="agglomeration", type=str,
                        default="median", choices=("median", "mean"),
                        help="Plot mean or median")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true",
                        default=False,
                        help="print number of runs on plot")
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--train', dest="train",  default=False, action='store_true')
    group.add_argument(
        '--test', dest="test", default=True, action='store_true')

    args = parser.parse_args()

    # Get files and names
    if args.test:
        all_file_list = glob.glob(
            "%s/*/run-*/validate-time-test/validationResults*-traj*-"
            "*time*.csv" % args.scenario)
    else:
        all_file_list = glob.glob(
            "%s/*/run-*/validate-time-train/validationResults*-traj*-"
            "*time*.csv" % args.scenario)
    file_dict = {}
    for fn in all_file_list:
        ac = fn.split("/")[1]
        file_dict[ac] = file_dict.get(ac, [])
        file_dict[ac].append(fn)

    if len(file_dict) == 0:
        print("Could not find any file for %s" % args.scenario)
        return

    name_list = list(file_dict.keys())
    file_list = [file_dict[ac] for ac in name_list]

    for idx in range(len(name_list)):
        print("%20s contains %d file(s)" %
              (name_list[idx], len(file_list[idx])))

    if args.verbose:
        name_list = [name_list[i] + " (" + str(len(file_list[i])) + ")"
                     for i in range(len(name_list))]

    # Get data from csv
    performance = list()
    time_ = list()
    show_from = -sys.maxsize

    for name in range(len(name_list)):
        # We have a new experiment
        performance.append(list())
        for fl in file_list[name]:
            _none, csv_data = read_util.read_csv(fl, has_header=True)
            csv_data = np.array(csv_data)
            # Replace too high values with args.maxint
            if args.train:
                data = [min([args.maxvalue, float(i.strip())])
                        for i in csv_data[:, 1]]
            elif args.test:
                data = [min([args.maxvalue, float(i.strip())])
                        for i in csv_data[:, 2]]
            else:
                print("This should not happen")
            # do we have only non maxint data?
            show_from = max(data.count(args.maxvalue), show_from)
            performance[-1].append(data)
            time_.append([float(i.strip()) for i in csv_data[:, 0]])
            # Check whether we have the same times for all runs
            if len(time_) == 2:
                if time_[0] == time_[1]:
                    time_ = [time_[0], ]
                else:
                    raise NotImplementedError(
                            ".csv are not using the same times")
    performance = [np.array(i) for i in performance]
    time_ = np.array(time_).flatten()

    if args.train:
        print("Plot TRAIN performance")
    elif args.test:
        print("Plot TEST performance")
    else:
        print("Don't know what I'm printing")

    if args.xmin is None and show_from != 0:
        args.xmin = show_from

    new_time_list = [time_ for i in range(len(performance))]
    fig = plot_methods.\
        plot_optimization_trace_mult_exp(time_list=new_time_list,
                                         performance_list=performance,
                                         title=args.title,
                                         name_list=name_list,
                                         logx=args.logx, logy=args.logy,
                                         y_min=args.ymin, y_max=args.ymax,
                                         x_min=args.xmin, x_max=args.xmax,
                                         agglomeration="median")
    
    if args.save is None:
        save = "%s_perf_over_time.pdf" % args.scenario
    else:
        save = args.save
    fig.savefig(save)
    print("Saved figure as %s" % save)

if __name__ == "__main__":
    main()
