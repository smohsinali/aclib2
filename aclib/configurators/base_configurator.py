import abc

__author__ = "Marius Lindauer"
__version__ = "0.0.1"
__license__ = "BSD"


class BaseConfigurator(object):

    def __init__(self, aclib_root: str, suffix_dir: str=""):
        '''
            Constructor

            Arguments
            ---------
            aclib_root: str
                root directory to AClib
            suffix_dir : str
                suffix of AC procedure directory
        '''

        self.traj_file_regex = None
        self.suffix_dir = suffix_dir

    @abc.abstractmethod
    def get_call(self,
                 scenario_name: str,
                 seed: int=1,
                 ac_args: list=None,
                 exp_dir: str=".",
                 cores: int = 1):
        '''
            returns call to AC procedure for a given scenario and seed

            Arguments
            ---------
            scenario_name:str
                name of scenario
            seed: int
                random seed
            ac_args: list
                list of further arguments for AC procedure
            exp_dir: str
                experimental directory
            cores: int
                number of available cores

            Returns
            -------
                commandline_call: str
        '''
