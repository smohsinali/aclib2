import logging
import os
import glob

from aclib.configurators.base_configurator import BaseConfigurator
from aclib.configurators.utils.discretize import discretize_pcs

__author__ = "Marius Lindauer"
__version__ = "0.0.1"
__license__ = "BSD"


class ParamILS(BaseConfigurator):

    def __init__(self, aclib_root: str, suffix_dir: str=""):
        '''
            Constructor

            Arguments
            ---------
            aclib_root: str
                root directory to AClib
            suffix_dir : str
                suffix of AC procedure directory
        '''

        self.logger = logging.getLogger("ParamILS")

        self.traj_file_regex = "paramils_out/*traj_*.csv"

        self._bin = os.path.abspath(
            "%s/configurators/paramils%s/param_ils_2_3_run.rb" % (aclib_root, suffix_dir))

    def get_call(self,
                 scenario_name: str,
                 seed: int=1,
                 ac_args: list=None,
                 exp_dir: str=".",
                 cores: int = 1):
        '''
            returns call to AC procedure for a given scenario and seed

            Arguments
            ---------
            scenario_name:str
                name of scenario
            seed: int
                random seed
            ac_args: list
                list of further arguments for AC procedure
            exp_dir: str
                experimental directory
            cores: int
                number of available cores

            Returns
            -------
                commandline_call: str
        '''

        print("%s/scenarios/*/%s/scenario.txt" % (exp_dir,scenario_name))
        scenario_fn = glob.glob(
            "%s/scenarios/*/%s/scenario.txt" % (exp_dir,scenario_name))[0]
            
        pcs_fn = self._rewrite_scenario(scenario_fn=scenario_fn,
                               exp_dir=exp_dir)
        
        discretize_pcs(pcs_fn=os.path.join(exp_dir,pcs_fn), granularity=7, fn_out=os.path.join(exp_dir, "disc_pcs.txt"))

        cmd = "%s -scenariofile scenario.txt -numRun %d -validN 0 1> log-%d.txt 2>&1" % (
            self._bin, seed, seed)
        if ac_args:
            cmd += " " + " ".join(ac_args)

        return cmd

    def _rewrite_scenario(self, scenario_fn: str, exp_dir: str):
        '''
            copy scenario file and replace pcs file

            Arguments
            ---------
            scenario_fn: str
                scenario file name
            exp_dir: str
                experimental directory

            Returns
            -------
            fn : str
                original pcs file name
        '''

        pcs_fn = None
        with open(os.path.join(exp_dir, "scenario.txt"), "w") as out_fp:
            with open(scenario_fn) as fp:
                for line in fp:
                    line = line.replace("\n", "").strip(" ")
                    if line.startswith("pcs-file") or \
                            line.startswith("param-file") or \
                            line.startswith("paramFile") or \
                            line.startswith("paramfile"):

                        pcs_fn = line.split("=")[1].strip(" ")
                        out_fp.write("paramfile=disc_pcs.txt\n")
                    else:
                        out_fp.write(line + "\n")

        if pcs_fn is None:
            sys.stderr.write(
                "PCS file not found in scenario file (%s)" % (scen_fn))
            sys.exit(44)

        return pcs_fn
