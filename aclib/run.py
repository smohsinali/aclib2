#!/usr/bin/env python3.5
# encoding: utf-8

import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType

# hack to avoid installing of Aclib
import sys
import os
import inspect
cmd_folder = os.path.realpath(
    os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from aclib.configurators.ac_interface import ACInterface

from aclib.job_systems.meta_cluster import MetaCluster
from aclib.job_systems.local_system import LocalSystem

__author__ = "Marius Lindauer and Katharina Eggensperger"
__version__ = "0.0.1"
__license__ = "BSD"


class AClibRun(object):

    def __init__(self):
        '''
            Constructor
        '''
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("AClibRun")

        self.aclib_root = os.path.abspath(
            os.path.split(os.path.split(__file__)[0])[0])

    def get_args(self):
        '''
            parses command line argument 
        '''

        parser = ArgumentParser(
            # version=__version__,
            formatter_class=ArgumentDefaultsHelpFormatter)
        req_group = parser.add_argument_group("Required")

        req_group.add_argument("-s", "--scenario", required=True,
                               help="Scenario name; see \"/scenarios/<domain>/<scenario name>\"")
        req_group.add_argument("-c", "--configurator", required=True,
                               choices=["SMAC2", "SMAC3", "PARAMILS", "GGA", "ROAR"], help="algorithm configuration procedure")

        adv_group = parser.add_argument_group("Advanced")
        adv_group.add_argument(
            "--suffix", default="", help="suffix of AC procedure directory")
        adv_group.add_argument(
            "--ac_cores", type=int, default=1, help="number of AC procedure CPU cores (only applicable for GGA at the moment)")

        exec_group = parser.add_argument_group("Execution")
        exec_group.add_argument(
            "--env", choices=["local", "meta"], default="meta", help="environment to execute runs")
        exec_group.add_argument(
            "-n", "--number_of_runs", type=int, default=1, help="number of AC runs in parallel")
        exec_group.add_argument(
            "--cores_per_job", type=int, default=1, help="number of CPU cores per job")
        exec_group.add_argument(
            "--startup", dest="startup", type=str, default=None,
            help="File with commands to execute before running commands. "
                 "Ignored when '--env local'")

        return parser.parse_known_args()

    def main(self):
        '''
            main method
        '''

        args_, ac_args = self.get_args()

        exp_dir = "%s/%s" % (args_.scenario, args_.configurator)

        if args_.suffix:
            exp_dir += args_.suffix

        if ac_args:
            exp_dir += "_" + "_".join(ac_args)


        bac = ACInterface(aclib_root=self.aclib_root, suffix_dir=args_.suffix)
        ac = bac.get_AC(ac_name=args_.configurator)

        ac_cmds = []
        for seed in range(1, args_.number_of_runs + 1):
            exp_seed_dir = exp_dir + "/run-%d" % (seed)
            self._create_exp_dir(path=exp_seed_dir)

            ac_cmd = ac.get_call(
                scenario_name=args_.scenario, seed=seed, ac_args=ac_args, exp_dir=exp_seed_dir, cores=args_.ac_cores)
            self.logger.debug(ac_cmd)
            ac_cmds.append("cd \"%s\"; %s" %
                           (os.path.abspath(exp_seed_dir), ac_cmd))

        self.submit(cmds=ac_cmds, system=args_.env,
                    exp_dir=exp_dir, cores_per_job=args_.cores_per_job,
                    startup=args_.startup)

    def _create_exp_dir(self, path: str):
        '''
            creates an directory with symlinks to AClib 

            Arguments
            ---------
            path : str
                directory path for experiments
        '''
        os.makedirs(path, exist_ok=True)

        cwd = os.getcwd()
        os.chdir(path)

        scenarios_path = os.path.join(self.aclib_root, "scenarios")
        algorithms_path = os.path.join(self.aclib_root, "target_algorithms")
        instances_path = os.path.join(self.aclib_root, "instances")
        configurators_path = os.path.join(self.aclib_root, "configurators")

        if not os.path.exists("./scenarios"):
            try:
                os.symlink(scenarios_path, "./scenarios")
            except OSError:
                self.logger.warn(
                    "File was created. Only relevant if NOT using pSMAC")
        else:
            self.logger.warn(
                "./scenarios existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./target_algorithms"):
            try:
                os.symlink(algorithms_path, "./target_algorithms")
            except OSError:
                self.logger.warn(
                    "File was created. Only relevant if NOT using pSMAC")
        else:
            self.logger.warn(
                "./target_algorithms existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./instances"):
            try:
                os.symlink(instances_path, "./instances")
            except OSError:
                self.logger.warn(
                    "[WARNING] File was created. Only relevant if NOT using pSMAC")
        else:
            self.logger.warn(
                "./instances existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./configurators"):
            try:
                os.symlink(configurators_path, "./configurators")
            except OSError:
                self.logger.warn(
                    "File was created. Only relevant if NOT using pSMAC")
        else:
            self.logger.warn(
                "./configurators existed in working directory. Assuming correct symlink...")

        os.chdir(cwd)

    def submit(self, cmds: list, system: str, exp_dir: str, cores_per_job: int,
               startup: str=None):
        '''
            submits/runs list of command line calls

            Arguments
            ---------
            cmds: list
                command line calls
            system: str
                system to run command on
            exp_dir: str
                experiment directory
            cores_per_job: int
                number of cores per job
        '''

        if system == "meta":
            env = MetaCluster()
            env.run(exp_dir=exp_dir, cmds=cmds,
                    cores_per_job=cores_per_job, job_cutoff=172800,
                    startup_fl=startup)
        elif system == "local":
            env = LocalSystem()
            env.run(exp_dir=exp_dir, cmds=cmds,
                    cores_per_job=cores_per_job, job_cutoff=172800)


if __name__ == "__main__":
    aclib = AClibRun()
    aclib.main()
